# Open source / Free sofware

## Newsletter

- https://mailtrain.org/

## No-code database and Airtable alternative

- https://www.nocodb.com/
- https://gitlab.com/bramw/baserow

## Slides

- [Slidev/](https://sli.dev/)
  - https://github.com/slidevjs/slidev

## Community

- [Community Canvas](https://community-canvas.org/)

## Github / Gitlab templates

- [restic: .github/ISSUE_TEMPLATE/Bug.md](https://raw.githubusercontent.com/restic/restic/master/.github/ISSUE_TEMPLATE/Bug.md)
- [restic: .github/ISSUE_TEMPLATE/Feature.md](https://raw.githubusercontent.com/restic/restic/master/.github/ISSUE_TEMPLATE/Feature.md)
- [restic: .github/PULL_REQUEST_TEMPLATE.md](https://raw.githubusercontent.com/restic/restic/master/.github/PULL_REQUEST_TEMPLATE.md)

## Backers / Sponsors

"Giving funds by becoming a sponsor on Open Collective or ..."

- https://www.helloasso.com
- **OpenCollective** - Online funding platform for open and transparent communities
  - https://opencollective.com/
  - https://github.com/opencollective/opencollective

examples: 
- "Thank you to all our backers! Become a backer"
- "Support this project by becoming a sponsor. Your logo will show up here with a link to your website. Become a sponsor"
  - https://github.com/MichaelMure/git-bug/blob/master/README.md#backers
  - https://github.com/MichaelMure/git-bug/blob/master/README.md#sponsors
