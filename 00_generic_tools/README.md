# Generic tools

## Temporary Email address

- https://temp-mail.org/fr/


## Email Aliasing

- https://www.privacyguides.org/fr/email-aliasing/
- https://addy.io/
- https://simplelogin.io/
- https://relay.firefox.com/ 

## Check if mail server is well configured (SPF, DNS, ...)

- https://www.mail-tester.com/


## Icones

- https://thenounproject.com/

## Generates editor themes, terminal theme

- https://themer.dev
  - https://github.com/mjswensen/themer

 ## Editor Markdown

 - [Ghostwriter](https://github.com/wereturtle/ghostwriter)
 - [Zettlr](https://github.com/Zettlr/Zettlr)
   - [retour utilisateur](https://golb.statium.link/post/20200218zettlr/)

## Dataviz (data visualisations)

- How to select the optimal symbology for data visualisations, by the Financial Times Visual Journalism Team.
  - https://github.com/ft-interactive/chart-doctor/blob/master/visual-vocabulary/Visual-vocabulary.pdf
  - http://ft-interactive.github.io/visual-vocabulary/
- [data-to-viz.com](https://www.data-to-viz.com)
- [datavizproject.com](https://datavizproject.com)
- Livre [Fundamentals of Data Visualization](https://clauswilke.com/dataviz/), Claus O. Wilke
- outil [Observable Plot](https://github.com/observablehq/plot) --> "JavaScript library for exploratory data visualization. "  

## Data manipulation / visualization tools

https://sq.io --> not yet tested 
> sq is a Swiss army knife to inspect, query, join, import and export data. You can think of it as jq for relational data, whether that data is in a document or a database. 

https://www.visidata.org ----> powerful, but many keyboard shortcuts to learn
> VisiData is an interactive tool for tabular data. It combines the clarity of a spreadsheet, the efficiency of a terminal and the power of Python, in a lightweight utility that can handle millions of rows with ease.

https://datasette.io  ----> not yet tested, to publish data in open-data?
> Datasette is a data exploration and publishing tool. It helps people take data of any form, analyze and explore it, and then publish it as an interactive website and corresponding API.
> - demo : https://datasette-dashboards-demo.vercel.app/jobs/offers
>   - dashboards (via an experimental plugin) : https://datasette-dashboards-demo.vercel.app/-/dashboards/job-offers-stats

## Creating diagrams from text descriptions

- **Mermaid** (open source, Javascript based diagramming and charting tool that uses Markdown-inspired text definitions)
    - https://github.com/mermaid-js/mermaid
    - https://mermaid-js.github.io
- [diagram.codes](https://www.diagram.codes) (online service)
- **Kroki** (open source + online service + API)
    - [kroki.io](kroki.io)
    - [github.com/yuzutech/kroki](https://github.com/yuzutech/kroki)
- **PlantUML** (open source + online service)
    - [plantuml.com](https://plantuml.com/fr/)
    - [Demo](http://www.plantuml.com/plantuml/uml/)
    - [enable PlantUML in GitLab](https://docs.gitlab.com/ee/administration/integration/plantuml.html)

## Creating diagrams 

- [Archi](https://www.archimatetool.com/) (open source)
- [Dia](http://dia-installer.de/)  (open source)

## Creating diagrams 

- **Excalidraw** (virtual whiteboard)
  - [Excalidraw.com](https://excalidraw.com/)  (online service)
  - https://github.com/excalidraw/excalidraw  (open source, but use Google Firebase))


### Creating Mindmaps from Markdown files
- **Markmap** (open source + online service)
  - [markmap.js.org](https://markmap.js.org/)
  - [github.com/gera2ld/markmap-lib](https://github.com/gera2ld/markmap-lib)
  - [Demo](https://markmap.js.org/repl)  ---> Live edit + export SVG or interactive HTML

### French fake tel number

> Ces racines de numéros de téléphone fictives, réservées pour le cinéma, peuvent être utilisées comme exemples de saisie dans vos formulaires :
> 
> - 01 99 00
> - 02 61 91
> - 03 53 01
> - 04 65 71
> - 05 36 49
> - 06 39 98
> 
> cf. https://legifrance.gouv.fr/jorf/article_jo/JORFARTI000037263033 (article 2.5.12)
>
> Ces numéros ne peuvent :
> - ni faire l'objet d'attribution par l'ARCEP ;
> - ni être affectés à des utilisateurs finals ;
> - ni être utilisés en tant qu'identifiant de l'appelant présenté à l'appelé ;
> - ni être appelés par des utilisateurs finals.
> 
> soit 5 x 10 0000  numéros de téléphones qui n'existerons jamais.

> usage :
> - exemple pour les formulaire
> - exemple dans de la documentation
> - fixtures pour les tests
> - anonymisation de données




## Temporary SMS number

- https://temp-number.org/
- ...
