

## source : dcpc.info

en anglais : "Best Practices Guide for Digital Commons – Government Relations" (septembre 2024)
https://dcpc.info/publications/best-practices-guide-for-digital-commons-government-relations/

"Rapport sur l’action du digital commons policy council en faveur de la reconnaissance des communs numeriques" (mai 2024, en français)
https://dcpc.info/publications/rapport-sur-laction-du-digital-commons-policy-council-en-faveur-de-la-reconnaissance-des-communs-numeriques/
