# Remote Working

- [FR - Framasoft - Mémo pour télétravail (libre)](https://framasoft.frama.io/teletravail/) 
- [FR - Guide ultime du télétravail](https://www.notion.so/Le-guide-ultime-du-t-l-travail-d255c513ab224ee4b70d10426baf0858)
- [FR - Conseils pour le développeur remote](https://alexsoyes.com/developpeur-remote/) by alexsoyes.com
- [GitLab's Guide to All-Remote](https://about.gitlab.com/company/culture/all-remote/guide/)

