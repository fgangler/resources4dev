# Bill of Materials (BOM)



## OWASP CycloneDX

- https://cyclonedx.org
- https://github.com/CycloneDX
- https://owasp.org/www-project-cyclonedx/

> OWASP CycloneDX is a full-stack Bill of Materials (BOM) standard that provides advanced supply chain capabilities for cyber risk reduction. The specification supports:
> - Software Bill of Materials (SBOM)
> - Software-as-a-Service Bill of Materials (SaaSBOM)
> - Hardware Bill of Materials (HBOM)
> - Machine Learning Bill of Materials (ML-BOM)
> - Cryptography Bill of Materials (CBOM)
> - Manufacturing Bill of Materials (MBOM)
> - Operations Bill of Materials (OBOM)
> - Vulnerability Disclosure Reports (VDR)
> - Vulnerability Exploitability eXchange (VEX)
> - CycloneDX Attestations (CDXA)


## Continuous SBOM analysis platform

### Dependency-Track

- https://dependencytrack.org
- https://github.com/DependencyTrack/dependency-track

## Ressources

- [FR - Cartographier et analyser la sécurité des dépendances logicielles en continu avec des outils open source](https://medium.com/@fablev/cartographier-et-analyser-la-s%C3%A9curit%C3%A9-des-d%C3%A9pendances-logicielles-avec-des-outils-open-source-c0d8a151dd60) : Trivy + Gitlab CI + Dependency-Track + example (Symfony demo application)
- [FR - Pourquoi le SBOM est considéré comme un allié indispensable de la sécurité de vos produits ?](https://www.riskinsight-wavestone.com/2024/03/pourquoi-le-sbom-est-considere-comme-un-allie-indispensable-de-la-securite-de-vos-produits/)

