# DNS

## Modules auto-formation AFNIC


[Configuration de zone DNS](https://www.afnic.fr/observatoire-ressources/actualites/configuration-de-zone-dns-lafnic-lance-un-parcours-de-formation-gratuit-en-5-videos/)
- [Module 1 : La désynchronisation de zone DNS](https://www.afnic.fr/produits-services/formations/programme-formation-video-5-tests-pour-reussir-votre-configuration-dns/module-1/)
  - [Vidéo](https://www.youtube.com/watch?v=99g0jzFSlYc)
  - [Quizz](https://p8segn08c6i.typeform.com/to/B4CvZhRW)
- [Module 2 : Valider la configuration de vos entrées MX dans votre zone DNS](https://www.afnic.fr/produits-services/formations/programme-formation-video-5-tests-pour-reussir-votre-configuration-dns/module-2/)
  - [Vidéo](https://www.youtube.com/watch?v=p04jFV7RWAM)
  - [Quizz](https://p8segn08c6i.typeform.com/to/pGhCSxOu)
- [Module 3 : Tester le bon fonctionnement de votre zone DNS avec DNSSEC](https://www.afnic.fr/produits-services/formations/programme-formation-video-5-tests-pour-reussir-votre-configuration-dns/module-3/)
  - [Vidéo](https://www.youtube.com/watch?v=DJA9KoHlw6o)
  - [Quizz](https://p8segn08c6i.typeform.com/to/cdKwihBh)
- [Module 4 : Les erreurs à ne pas commettre dans les enregistrements CNAME, NS, MX et SRV](https://www.afnic.fr/produits-services/formations/programme-formation-video-5-tests-pour-reussir-votre-configuration-dns/module-4/)
  - [Vidéo](https://www.youtube.com/watch?v=m8Mtu8vKlPk)
  - [Quizz](https://p8segn08c6i.typeform.com/to/UREtbrUN)
- Module 5 : Comprendre la réjuvénation DNS
  - [Vidéo](https://www.youtube.com/watch?v=V5IdYqxp248) 
  - [Quizz](https://p8segn08c6i.typeform.com/to/DL9LLu0d)


