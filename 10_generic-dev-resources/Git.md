# GIT resources

## GIT resources in English

- [Git: Cheat Sheet (advanced)](https://dev.to/maxpou/git-cheat-sheet-advanced-3a17)
- [Learn git one commit at a time](http://gitready.com/)
- [Git Command Explorer](https://gitexplorer.com/) 
- Interactive : 
  - [Interactive zone visualization](http://ndpsoftware.com/git-cheatsheet.html) 
  - Interactive visualization of command line action:
    - [Explain Git with D3](https://onlywei.github.io/explain-git-with-d3/) 
    - [Learning Git branching](https://learngitbranching.js.org/)  (examples are long to load at the beginning)
    - [Visualizing Git](http://git-school.github.io/visualizing-git/) 

## GIT resources in French

- Astuces "Zut, j'ai..." : https://ohshitgit.com/fr ou https://dangitgit.com/fr 
- [Le « Git Book » officiel](https://git-scm.com/book/fr/v2/)
- [Vidéos - Glossaire Git](https://www.youtube.com/playlist?list=PLPoAfTkDs_JZ_DDl5MReG67N1tF9s0Z3i)
- [Vidéos - Les concepts clés de Git](https://www.youtube.com/playlist?list=PLPoAfTkDs_JbMPY-BXzud0aMhKKhcstRc)
- Articles:
  - [Workflow Git : objectifs et principes généraux](https://delicious-insights.com/fr/articles/git-workflows-generality/)
  - [Git log pour analyser l’historique des commits](https://delicious-insights.com/fr/articles/git-log/)
  - [Bien utiliser Git merge et rebase](https://delicious-insights.com/fr/articles/bien-utiliser-git-merge-et-rebase/)
  - [Git reset : rien ne se perd, tout se transforme](https://delicious-insights.com/fr/articles/git-reset/)
- Videos:
  - [Conférence "Git reset : rien ne se perd, tout se transforme"](https://www.youtube.com/watch?v=EgNTvj-z2Gc)
  - [Défaire des modifications en cours](https://cours-video.delicious-insights.com/courses/git-undo/109263-defaire-avant-d-avoir-commite/318666-defaire-des-modifications-en-cours)
  - [Défaire une modification dans la copie de travail](https://cours-video.delicious-insights.com/courses/git-undo/109263-defaire-avant-d-avoir-commite/318667-defaire-une-modification-dans-la-copie-de-travail)
  - [Défaire une modification dans l'index et la copie de travail](https://cours-video.delicious-insights.com/courses/git-undo/109263-defaire-avant-d-avoir-commite/318668-defaire-une-modification-dans-l-index-et-la-copie-de-travail)
  - [Défaire une modification seulement dans l'index](https://cours-video.delicious-insights.com/courses/git-undo/109263-defaire-avant-d-avoir-commite/318669-defaire-une-modification-seulement-dans-l-index)
  - [Défaire des commits ](https://cours-video.delicious-insights.com/courses/git-undo/109268-defaire-des-commits/318677-toujours-avec-reset-mais)
  - [Argh ! Ma fusion aurait dû attendre, je veux défaire mon merge](https://cours-video.delicious-insights.com/courses/git-merge-vs-rebase/136075-fusionner-intelligemment-une-branche/401076-argh-ma-fusion-aurait-du-attendre-je-veux-defaire-mon-merge)
  - [Découper un commit](https://cours-video.delicious-insights.com/courses/git-merge-vs-rebase/136078-soigner-mon-historique-avec-rebase/401112-decouper-un-commit)
  - [Oh non, ma branche part du mauvais endroit !](https://cours-video.delicious-insights.com/courses/git-merge-vs-rebase/136141-cas-pratiques-d-utilisation-de-rebase/401119-oh-non-ma-branche-part-du-mauvais-endroit)


## Other English ressources:

- [gitconfig.ini example](https://gist.github.com/tdd/470582)
- https://twitter.com/smashingmag/status/1222517905931325442
