# Generic resources for web development

- [**Git** resources](./Git.md)

----------------------------

- [Gitmoji: emoji guide for commit messages](https://gitmoji.carloscuesta.me/)
- [Learn X in Y minutes](https://learnxinyminutes.com/)
- [I don't know how to name my function](https://namingmyfunction.vercel.app/)
- Big-O Notation 
  - [Big-O Notation cheatsheet](https://www.bigocheatsheet.com/)
  - [Big-O notation explained by a self-taught programmer](https://justin.abrah.ms/computer-science/big-o-notation-explained.html)


## Algorithms implemented 

- https://the-algorithms.com/fr
  - https://github.com/TheAlgorithms/Rust
  - https://github.com/TheAlgorithms/PHP
  - https://github.com/TheAlgorithms/Python
  - ...


## Éco-conception

- [Éco-conception : outils et ressources pour designers](https://blog.hello-bokeh.fr/2021/11/25/eco-conception-outils-et-ressources-pour-designers/)

##  Dev mail catcher

- https://github.com/mailhog/MailHog  (Go)
- https://mailcatcher.me/ (Ruby)
- https://nodemailer.com/ (NodeJs)
  - https://github.com/nodemailer/nodemailer
  - service (gratuit) : https://ethereal.email
- https://nodemailer.com/app/  (NodeJs desktop app ---> /!\ not free software / OSS)
  - https://github.com/nodemailer/nodemailer-app ---> /!\ not free software / OSS
- services :
  - https://usehelo.com/

## Expose local websites via secure tunnels 

- https://ngrok.com/ (online service, not open source software)
- Expose (european online service, open-source) ---> prerequisite PHP
  - https://expose.dev
  - https://github.com/beyondcode/expose
- not tested:
  - PageKite (online service, open-source) https://pagekite.net/wiki/OpenSource/
  - Sish (open-source) https://github.com/antoniomika/sish


## Ports Database
- [official and unofficial tcp/udp port](https://www.speedguide.net/port.php?port=8568)

## Security
- [ ] [Overview of web security (Stanford course)](https://web.stanford.edu/class/cs253/) - Most common web attacks and their countermeasures

## Best practices

- https://conventionalcomments.org/
- [Writing system software: code comments.](http://antirez.com/news/124)
- [ ] [Twelve-Factor-App ---> 12factor.net](https://12factor.net/fr/) - Methodology for building software-as-a-service apps
- [ ] [Refactoring.guru](https://refactoring.guru/) - Refactoring, design patterns, SOLID principles, and other smart programming topics)

## Regular expression (Regex)

- [Regex101.com](https://regex101.com/) - Online regex tester, debugger with highlighting for PHP, PCRE, Python, Golang and JavaScript.
- [Regexr.com](https://regexr.com) - Online regex tester, PHP / PCRE & JS Support, contextual help, cheat sheet, reference, and searchable community patterns.
- [Visual Guide to Regular Expression](https://amitness.com/regex/)

## Json file
- Command-line JSON processor:
  - **JQ**
    - https://stedolan.github.io/jq/
    - [online playground for jq](https://jqplay.org)

## Git information extractor

Command-line Git information tool written in Rust
https://github.com/o2sh/onefetch
