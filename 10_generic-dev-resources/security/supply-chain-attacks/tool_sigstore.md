# Sigstore

- signing digital software artifacts
- signatures are bound to a public identity instead of a public key
- signatures are logged publicly for verification

https://www.sigstore.dev
- sigtore Rekor  - Signature transparency log 
- sigtore Fulcio - Free Certificate Authority
- sigtore Cosign - CLI to sign and verify artifacts


## Ressources

- FR Podcast "No Limit Secu" https://www.nolimitsecu.fr/sigstore/
- [Video EN, Introduction to Sigstore: cryptographic signatures made easier](https://passthesalt.ubicast.tv/videos/2023-introduction-to-sigstore-cryptographic-signatures-made-easier/) ("Pass the SALT" Conference)
  - https://archives.pass-the-salt.org/Pass%20the%20SALT/2023/slides/PTS2023-Talk-12-Introduction-to-Sigstore_Cryptographic-signatures-made-easier.pdf
