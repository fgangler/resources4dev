# Database


# Postgres

- https://www.postgresql.org/docs/
- Postgres Observability https://pgstats.dev

## Outils

DBeaver
- https://dbeaver.io/
- https://github.com/dbeaver/dbeaver

### Outils à tester

- https://github.com/azimuttapp/azimutt

### Psql (CLI)

Listing Databases (
```bash
\l
\list
```

Switching **Databases**
```bash
\c        databaseName
\connect  databaseName
```

Listing **Tables** 
```bash
\dt 
\dt+
```

List **Columns** of a specific table
```bash
\d   tableName
\d+  tableName
```