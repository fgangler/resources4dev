
# Puppet

- [Puppet Forge](https://forge.puppet.com/)
- [Puppet documentation](https://puppet.com/docs/puppet/latest/puppet_index.html)
- [Vox Pupuli documentation](https://voxpupuli.org/docs/)
- [Puppet Community](https://slack.puppet.com/) (slack)


## Learn Puppet

- [Puppet Language Basics](https://puppet.com/learning-training/kits/puppet-language-basics/)
- [Training catalog](https://learn.puppet.com/course-catalog)

## IDE / Editeur

- IntelliJ (completely outdated with Puppet)
  - https://plugins.jetbrains.com/plugin/7180-puppet
  - see also https://youtrack.jetbrains.com/issue/RUBY-23761
- not yet tested
  - Puppet VSCode Extension - https://puppet-vscode.github.io/ et https://github.com/lingua-pupuli
  - VIM - https://voxpupuli.org/blog/2019/04/08/puppet-lsp-vim/
    - Puppet Editor Services - https://github.com/puppetlabs/puppet-editor-services  

```shell
# not yet tested
git clone git@github.com:lingua-pupuli/puppet-editor-services.git
```

## PDK (Puppet Development Kit)

```shell
# create a new module with PDK
pdk new module asqatasun

# run unit test
pdk test unit -v

# linter
pdk validate
```
see: 
- [Download PDK (Puppet Development Kit)](https://puppet.com/try-puppet/puppet-development-kit/) 
- [PDK documentation](https://puppet.com/docs/pdk/1.x/pdk.html)

## Generate REFERENCE.md file

The `REFERENCE.md` file must be updated automatically using the following command line :

```shell
puppet strings generate --format markdown --out REFERENCE.md
```
### Install Puppet and puppet-strings 

```shell
# Install Puppet
wget https://apt.puppetlabs.com/puppet6-release-bionic.deb
sudo dpkg -i puppet6-release-bionic.deb
sudo apt install puppet
puppet --version

# Install puppet-common (necessary?)
sudo apt install puppet-common

# Install puppet-strings in the same ruby context as puppet
whereis puppet # /usr/bin/puppet
whereis puppet # /usr/bin/gem
sudo /usr/bin/gem install puppet-strings
```

##  Run puppet on custom manifest (usage: POC)

```shell
# Display Ruby and Puppet versions
which ruby
which puppet
puppet --version
ruby --version

# Run puppet on custom manifest (usage: POC)
cd manifests/
puppet apply test.pp
```

##  Run acceptance tests 

```shell

# Display Docker, Ruby and Puppet versions
which ruby
which puppet
which docker
puppet --version
ruby --version
docker -v

# Prerequisites to run acceptance tests
bundle install --path ./vendor
	# bundle update --bundler
	# gem install bundler:2.1.4

# Run acceptance tests on Ubuntu 16.04
BEAKER_PUPPET_COLLECTION='puppet5'     \
PUPPET_INSTALL_TYPE='agent'            \
BEAKER_IS_PE='no'                      \
BEAKER_debug='true'                    \
BEAKER_setfile='ubuntu1604-64'         \
BEAKER_HYPERVISOR='docker'             \
bundle exec rake beaker

# Run acceptance tests on Ubuntu 18.04
BEAKER_PUPPET_COLLECTION='puppet5'     \
PUPPET_INSTALL_TYPE='agent'            \
BEAKER_IS_PE='no'                      \
BEAKER_debug='true'                    \
BEAKER_setfile='ubuntu1804-64'         \
BEAKER_HYPERVISOR='docker'             \
bundle exec rake beaker

# Run acceptance tests on Ubuntu 18.04
# but not destroy Docker container
BEAKER_destroy=no                      \
BEAKER_PUPPET_COLLECTION='puppet5'     \
PUPPET_INSTALL_TYPE='agent'            \
BEAKER_IS_PE='no'                      \
BEAKER_debug='true'                    \
BEAKER_setfile='ubuntu1804-64'         \
BEAKER_HYPERVISOR='docker'             \
bundle exec rake beaker
```


## Puppet Agent logs

```shell
sudo su -
cat /var/log/syslog | grep puppet
```

## CLI puppet agent

### enable/disable puppet agent
```shell
sudo su -

# disable puppet agent
puppet agent --disable "your message for monitoring sysadmin" --verbose

# enable puppet agent
puppet agent --enable --verbose

# checks puppet agent status
cat `puppet agent --configprint agent_disabled_lockfile` # file only exist when puppet agent is disabled
cat /var/log/syslog | grep puppet-agent
```

### Apply latest catalog immediately

- Puppet agent is configured to run at a specific interval. Default is 30 minutes. 
- `--test` option creating an override to pull and apply latest catalog immediately

```shell
sudo su -

# Pull and apply latest catalog immediately
puppet agent --test
```



## Modules

example:
- https://gitlab.adullact.net/adullact/puppet-coturn
- https://gitlab.adullact.net/adullact/puppet-freeipa
- https://gitlab.adullact.net/demarches-simplifiees/puppet-demarchessimplifiees
- Test fonctionnel (CURL, Chrome)
  - https://gitlab.adullact.net/adullact-puppet/app_etherpad/-/blob/master/spec/acceptance/app_etherpad_spec.rb


### Puppet Archive

- https://forge.puppet.com/modules/puppet/archive
- https://github.com/voxpupuli/puppet-archive

### Puppet JAVA

- https://forge.puppet.com/modules/puppetlabs/java
- https://github.com/puppetlabs/puppetlabs-java

### Puppet MySQL

- https://forge.puppet.com/modules/puppetlabs/mysql
- https://github.com/puppetlabs/puppetlabs-mysql



