# Bash scripting

## Rust tools

- [BAT, a cat clone with syntax highlighting and Git integration](https://github.com/sharkdp/bat)
- no tested yet:
  - [BROOT, a directory browser CLI](https://github.com/Canop/broot)
  - [DOG, a rust DNS client CLI](https://github.com/ogham/dog)

## Sheatsheets

- [Bash scripting cheatsheet](https://devhints.io/bash) _(devhints.io)_
  - [Rsync cheatsheet](https://devhints.io/rsync)

## Learn

- [Learn bash in y minutes](https://learnxinyminutes.com/docs/bash/)  _(learnxinyminutes.com)_

### Advanced bash scripting
- [Advanced Bash-Scripting Guide](http://tldp.org/LDP/abs/html/)
- [Writing Robust Bash Shell Scripts](https://www.davidpashley.com/articles/writing-robust-shell-scripts/)
- [Bash best practices](https://bertvv.github.io/cheat-sheets/Bash.html)
- [Curated list of delightful Bash scripts and resources](https://github.com/awesome-lists/awesome-bash)

### Defensive BASH Programming
- [Defensive BASH Programming](http://kfirlavi.herokuapp.com/blog/2012/11/14/defensive-bash-programming/)
  - [Markdown version](https://jonlabelle.com/snippets/view/markdown/defensive-bash-programming)
  - [FR - Programmation défensive en bash](https://blog.seboss666.info/2020/04/programmation-defensive-en-bash/)

## Tools

- [pgrep,  pkill](http://manpages.ubuntu.com/manpages/xenial/fr/man1/pgrep.1.html) - rechercher ou envoyer un signal à des processus en fonction de leur nom et  d'autres propriétés

### Extra tools

```bash
sudo apt install bat
sudo apt install httpie 
sudo apt install ccze
sudo apt install shellcheck 
```

#### Check network usage per process

- **IPTraf**: network monitoring tool 
- **iftop**: produces a frequently updated list of network connections
- **nethog**: monitor per process network bandwidth usage 

see: [Check network usage per process](https://linuxhint.com/network_usage_per_process/)

```bash
sudo apt install iptraf   
sudo apt install iftop
sudo apt install nethogs

sudo iptraf-ng 
sudo iftop -i <interfaceName>
sudo nethogs
```

#### ShellCheck

> The goals of **ShellCheck** are
> 
> - To point out and clarify typical beginner's syntax issues that cause a shell to give cryptic error messages.
> - To point out and clarify typical intermediate level semantic problems that cause a shell to behave strangely and counter-intuitively.
> - To point out subtle caveats, corner cases and pitfalls that may cause an advanced user's otherwise working script to fail under future circumstances.

- [Demo: shellcheck.net](https://www.shellcheck.net)
- [Plugin IntelliJ](https://plugins.jetbrains.com/plugin/10195-shellcheck)
- [github.com/koalaman/shellcheck](https://github.com/koalaman/shellcheck)
  - [Documentation](https://github.com/koalaman/shellcheck/wiki)
  - examples (untested yet) with Gitlab CI: https://gitlab.adullact.net/adullact/partage-gitlab-ci/-/issues/2


```bash
# Install
sudo apt install shellcheck

# Analyse one file
shellcheck testScript.sh

# Lint all shell scripts in a git repository
git ls-files --exclude='*.sh' --ignored | xargs shellcheck

```


#### shUnit2 (unit tests)

not tested yet:
- https://github.com/kward/shunit2


## Astuces

Options -B (before) et -A (after) de `grep` pour afficher des lignes de contextes : 
```bash
# affiche 2 lignes au dessus et 3 lignes en dessous de chaque ligne qui correspond au patern de recherche
grep -B2 -A3 "pageQuiNexistePas" /var/log/apache2/access.log  
```
