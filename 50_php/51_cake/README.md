# CakePHP

## Ressources

- [Authentification](https://book.cakephp.org/3/fr/controllers/components/authentication.html)


### DB query

- [Query Builder](https://book.cakephp.org/3/fr/orm/query-builder.html)
- [Retrieving Data & Results Sets](https://book.cakephp.org/3/en/orm/retrieving-data-and-resultsets.htm)
- Cake\ORM\Behavior
  - [Counter Cache behavior](https://book.cakephp.org/3/fr/orm/behaviors/counter-cache.html)
  - [Translate behavior](https://book.cakephp.org/3/fr/orm/behaviors/translate.html)

### Views

- [Helpers](https://book.cakephp.org/3/en/views/helpers.html)
  - [Accessing View Variables Inside Your Helper](https://book.cakephp.org/3/en/views/helpers.html#accessing-view-variables-inside-your-helper)
  - [Html](https://book.cakephp.org/3/en/views/helpers/html.html)
  - [Form](https://book.cakephp.org/3/en/views/helpers/form.html)
  - [Session](https://book.cakephp.org/3/fr/views/helpers/session.html)
  - [Text](https://book.cakephp.org/3/en/views/helpers/text.html)
    - [ ] [Converting Text into Paragraphs](https://book.cakephp.org/3/en/views/helpers/text.html#converting-text-into-paragraphs)
  - [ ] [Breadcrumbs](https://book.cakephp.org/3/fr/views/helpers/breadcrumbs.html)
  - [ ] [Rss](https://book.cakephp.org/3/en/views/helpers/rss.html)

