
# PHP

## PHP algorithms implemented 

- https://the-algorithms.com/fr
  - https://github.com/TheAlgorithms/PHP

## PHP 8.0

- https://www.php.net/releases/8.0/en.php
- [What's new in PHP 8](https://stitcher.io/blog/new-in-php-8) by stitcher.io
- [Smooth Upgrade to PHP 8 in Diffs](https://getrector.org/blog/2020/11/30/smooth-upgrade-to-php-8-in-diffs) by getrector.org
- [FR - Les nouvelles fonctionnalités de PHP 8](https://linuxfr.org/news/les-nouvelles-fonctionnalites-de-php-8) by linuxfr.org

## 

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php8.0
sudo update-alternatives --list php
sudo update-alternatives --config php
