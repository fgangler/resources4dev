# RUST 🦀

- [rust-lang.org](https://www.rust-lang.org/fr)
  - https://www.rust-lang.org/fr/learn
  - https://doc.rust-lang.org/book/
  - https://jimskapt.github.io/rust-book-fr/
- [Comprehensive Rust](https://google.github.io/comprehensive-rust/)
  > This is a four day Rust course developed by the Android team. The course covers the full spectrum of Rust, from basic syntax to advanced topics like generics and error handling. It also includes Android-specific content on the last day.

- https://lafor.ge/tags/rust/
- https://lafor.ge/tags/rust-par-le-metal/

## Memory 

- https://lafor.ge/rust/heap_stack/ 
- [FR, video / Sunny Tech 2023 - Dessine-moi Rust](https://www.youtube.com/watch?v=qJi1YCUy3nY)  
  - https://sunny-tech.io/sessions/dessinesmoi-rust-


## RUST algorithms implemented 

- https://the-algorithms.com/fr
  - https://github.com/TheAlgorithms/PHP