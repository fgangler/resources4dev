
# Python

## Ressources

- https://calmcode.io/  (Video tutorials: tools for Python, Python data tools, ...)
- https://www.w3schools.com/python/

## Gitlab CI

- https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml
- https://www.aerian.fr/en/english-gitlab-ci-python-linting/
- https://medium.com/semantixbr/how-to-make-your-code-shine-with-gitlab-ci-pipelines-48ade99192d1

## Panda library

- https://pandas.pydata.org/docs/reference/
  - https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.sort_values.html
  - https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_csv.html
- https://www.w3schools.com/python/pandas/
  - https://www.w3schools.com/python/pandas/trypandas.asp?filename=demo_pandas_csv


### Panda DataFrame to csv

- https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_csv.html
- https://queirozf.com/entries/pandas-dataframes-csv-quoting-and-escaping-strategies

## Convert JSON to CSV in Python

- https://www.geeksforgeeks.org/convert-nested-json-to-csv-in-python/

## How to convert Jupyter notebook to python

https://linuxhint.com/convert-jupyter-notebook-python/


## Pynsee package

> pynsee package contains tools to easily search and download French data from INSEE and IGN APIs.

- https://pynsee.readthedocs.io
- https://github.com/InseeFrLab/pynsee
- https://api.insee.fr/catalogue/
