
# Forum PHP 2020
- https://event.afup.org/forum-php-2020/programme-forum-php-2020/
- vote: https://joind.in/event/forum-php-2020/schedule/list
- jeux de l'oie
  - https://www.prestaconcept.net/medias/pdf/jeu_de_la_pull_couleur_by_prestaconcept.pdf
  - https://www.prestaconcept.net/medias/pdf/jeu_de_la_pull_noir_et_blanc_by_prestaconcept.pdf

## Visionné en live

- ""
- "JSON en base de données, manipulons un peu ça", J.VAN BELLE
  - https://github.com/dunglas/doctrine-json-odm
  - https://devhints.io/postgresql-json
-  "Comment perdre sa surcharge featurale ?", E.LANDRY ----> UX, à revoir
  - https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action?language=fr
- "Pourquoi 0.1 + 0.2 != 0.3...", B.JACQUEMONT
  - https://php-decimal.io/
- "Auto-critique de la revue de code bienveillante", Kim Lai Trinh
  - https://vimeo.com/379447609 (Paris Web 2019)
  - https://kimlaitrinh.me/
- "PHP 8.0: A new version, a new era", Gabriel Caruso
  - https://slides.com/carusogabriel/php8-en
- REX sur le chiffrement de base de données - Y.DURAND
  - lib PHP conseillé : https://github.com/paragonie/ciphersweet
  - https://www.sitepoint.com/how-to-search-on-securely-encrypted-database-fields/
- "Performance : mais que se passe-t-il après le backend", J.P.VINCENT
  - vidéo complémentaire : https://www.youtube.com/watch?v=DoBZAYZCFDs&feature=youtu.be
- "Trop de mock tue le test..." - J.M.LAMODIERE
  - https://github.com/JMLamodiere/tdd-demo-forumphp2020  ---> à explorer
  - http://www.natpryce.com/articles/000714.html
  - WireMock is a simulator for HTTP-based APIs
    - http://wiremock.org/
    - https://github.com/tomakehurst/wiremock

# à voir
- "État de l'art d'Elasticsearch avec PHP", @damienalexandre
  - https://jolicode.github.io/elasticsearch-php-conf/slides/forumphp2020.html#/
  - https://github.com/jolicode/elastically
