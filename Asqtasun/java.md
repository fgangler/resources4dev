
# Java

## Jsoup (CSS Selector)

- https://jsoup.org/apidocs/org/jsoup/select/Selector.html

- https://jsoup.org/cookbook/extracting-data/selector-syntax
- [Jsoup (Java Library) Cheatsheet](https://onecompiler.com/cheatsheets/jsoup)
- [Explain the JSOUP Select selector syntax](https://programmersought.com/article/33461335928/)
- [](https://o7planning.org/fr/10399/utiliser-java-jsoup-pour-analyser-html)
